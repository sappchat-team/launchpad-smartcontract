// We require the Hardhat Runtime Environment explicitly here. This is optional
// but useful for running the script in a standalone fashion through `node <script>`.
//
// When running the script with `npx hardhat run <script>` you'll find the Hardhat
// Runtime Environment's members available in the global scope.
const hre = require("hardhat");

async function main() {
  // Hardhat always runs the compile task when running scripts with its command
  // line interface.
  //
  // If this script is run directly using `node` you may want to call compile
  // manually to make sure everything is compiled
  // await hre.run('compile');

  // We get the contract to deploy

  //   PayBNNftMarketplace deployed to: 0xedCd84ec115803001f52936bF9D21f0990431b8e
  //   PayBNftPool deployed to: 0x492E4AaD2b583D6498CD5fd13d5809530Cd3D0EA
  //   PayBNNftBridgeTreasury deployed to: 0x12A04ca20B92494AF0de0941449c205Cc5D6304B
  const [deployer] = await hre.ethers.getSigners();

  console.log("Deploying contracts with the account:", deployer.address);

  const PayBNftMarketplace = await hre.ethers.getContractFactory(
    "PayBNftMarketplace"
  );
  const marketplace = new hre.ethers.Contract(
    "0xedCd84ec115803001f52936bF9D21f0990431b8e",
    PayBNftMarketplace.interface,
    deployer
  );

  // const PayBLaunchPad = await hre.ethers.getContractFactory("PayBLaunchPad");
  // console.log("deploying...");
  // const PayBNLaunchPad = await PayBLaunchPad.deploy(marketplace.address);
  // console.log("deployed...");
  // await PayBNLaunchPad.deployed();

  // console.log("Launchpad deployed to:", PayBNLaunchPad.address);

  // const Multicall = await hre.ethers.getContractFactory("Multicall");
  // const multicall = await Multicall.deploy();

  // await multicall.deployed();

  // console.log("Multicall deployed to:", multicall.address);

  // await marketplace.setPool("0x492E4AaD2b583D6498CD5fd13d5809530Cd3D0EA");
  await marketplace.setLaunchPad("0xb443fab284c3c71f9d4f410c9b63d38f6c5473de");
}

// We recommend this pattern to be able to use async/await everywhere
// and properly handle errors.
main().catch((error) => {
  console.error(error);
  process.exitCode = 1;
});
