// We require the Hardhat Runtime Environment explicitly here. This is optional
// but useful for running the script in a standalone fashion through `node <script>`.
//
// When running the script with `npx hardhat run <script>` you'll find the Hardhat
// Runtime Environment's members available in the global scope.
const hre = require("hardhat");

async function main() {
  // Hardhat always runs the compile task when running scripts with its command
  // line interface.
  //
  // If this script is run directly using `node` you may want to call compile
  // manually to make sure everything is compiled
  // await hre.run('compile');

  // We get the contract to deploy

  const [deployer] = await ethers.getSigners();

  console.log("Deploying contracts with the account:", deployer.address);

  const SAPPNftMarketplace = await hre.ethers.getContractFactory(
    "SAPPNftMarketplace"
  );
  const sAPPNNftMarketplace = await SAPPNftMarketplace.deploy();

  await sAPPNNftMarketplace.deployed();

  console.log(
    "SAPPNNftMarketplace deployed to:",
    sAPPNNftMarketplace.address
  );

  const SAPPNftPool = await hre.ethers.getContractFactory("SAPPNftPool");
  const sAPPNNftPool = await SAPPNftPool.deploy(
    sAPPNNftMarketplace.address,
    deployer.address
  );

  await sAPPNNftPool.deployed();

  console.log("SAPPNftPool deployed to:", sAPPNNftPool.address);

  const SAPPLaunchPad = await hre.ethers.getContractFactory("SAPPLaunchPad");
  const sAPPNLaunchPad = await SAPPLaunchPad.deploy(
    sAPPNNftMarketplace.address
  );

  await sAPPNLaunchPad.deployed();

  console.log("Launchpad deployed to:", sAPPNLaunchPad.address);

  const Multicall = await hre.ethers.getContractFactory("Multicall");
  const multicall = await Multicall.deploy();

  await multicall.deployed();

  console.log("Multicall deployed to:", multicall.address);
  
  await sAPPNNftMarketplace.setPool(sAPPNNftPool.address);
  await sAPPNNftMarketplace.setLaunchPad(sAPPNLaunchPad.address);
}

// We recommend this pattern to be able to use async/await everywhere
// and properly handle errors.
main().catch((error) => {
  console.error(error);
  process.exitCode = 1;
});
