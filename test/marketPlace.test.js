// We import Chai to use its asserting functions here.
const { ethers, waffle, network } = require("hardhat");

describe("Contract NFT marketplace", function () {
  let sAPPNftMarketplace;
  let sAPPNNftPool;
  let sAPPNftBridgeTreasury;
  let sAPPLaunchPad;

  let owner;
  let addr1;
  let addr2;
  let addrs;

  const ZERO_ADDRESS = "0x0000000000000000000000000000000000000000";

  beforeEach(async function () {
    [owner, addr1, addr2, ...addrs] = await ethers.getSigners();
    const SAPPNftMarketplace = await ethers.getContractFactory(
      "SAPPNftMarketplace"
    );
    sAPPNftMarketplace = await SAPPNftMarketplace.deploy();
    await sAPPNftMarketplace.deployed();

    const SAPPNftPool = await ethers.getContractFactory("SAPPNftPool");
    sAPPNNftPool = await SAPPNftPool.deploy(
      sAPPNftMarketplace.address,
      owner.address
    );
    await sAPPNNftPool.deployed();

    const SAPPNftBridgeTreasury = await ethers.getContractFactory(
      "SAPPNftBridgeTreasury"
    );
    sAPPNftBridgeTreasury = await SAPPNftBridgeTreasury.deploy(
      sAPPNftMarketplace.address
    );
    await sAPPNftBridgeTreasury.deployed();

    const SAPPLaunchPad = await ethers.getContractFactory("SAPPLaunchPad");
    sAPPLaunchPad = await SAPPLaunchPad.deploy(sAPPNftMarketplace.address);
    await sAPPLaunchPad.deployed();

    await sAPPNftMarketplace.setPool(sAPPNNftPool.address);
    await sAPPNftMarketplace.setLaunchPad(sAPPLaunchPad.address);

    console.log("SAPPNftMarketplace address", sAPPNftMarketplace.address);
    console.log("SAPPNNftPool address", sAPPNNftPool.address);
    console.log(
      "SAPPNftBridgeTreasury address",
      sAPPNftBridgeTreasury.address
    );
    console.log("SAPPLaunchPad address", sAPPLaunchPad.address);
    console.log("SAPPLaunchPad1 address", owner.address);
  });

  // Mint token (owner) => List token for sell (owner)=> Buy token (addr1)=>  List token for sell (addr1)=> Buy token (owner)
  it("Create, list for sell and buy NFT", async function () {
    await sAPPNftMarketplace.mint(
      owner.address,
      ZERO_ADDRESS,
      "URI",
      100,
      false,
      {
        from: owner.address,
      }
    );
    await sAPPNftMarketplace.setPriceAndSell(1, ethers.utils.parseEther("1"));

    await sAPPNftMarketplace.connect(addr1).buy(1, {
      value: ethers.utils.parseEther("1"),
    });

    await sAPPNftMarketplace
      .connect(addr1)
      .setPriceAndSell(1, ethers.utils.parseEther("1"));

    await sAPPNftMarketplace.connect(owner).buy(1, {
      value: ethers.utils.parseEther("1"),
    });
  });

  // Create a Launchpad => Owner stake to Launchpad => addr1 stake to launchpad => Admin set winner of launchpad => owner (winner) claim NFT => addr1 (loser) unstake
  it("Launchpad", async function () {
    const Token = await ethers.getContractFactory("TestERC20");
    const token = await Token.deploy();
    await token.deployed();
    await token.connect(addr1).mint();

    await sAPPNftMarketplace.mint(
      owner.address,
      ZERO_ADDRESS,
      "URI",
      100,
      true,
      {
        from: owner.address,
      }
    );
    await sAPPLaunchPad.addLaunchPad(
      "Token 1",
      token.address,
      1,
      1,
      Math.floor(new Date().getTime() / 1000),
      Math.floor(new Date().getTime() / 1000) + 1000,
      ethers.utils.parseEther("10")
    );
    await token.approve(
      sAPPLaunchPad.address,
      ethers.utils.parseEther("100000")
    );
    await token
      .connect(addr1)
      .approve(sAPPLaunchPad.address, ethers.utils.parseEther("100000"));
    await sAPPLaunchPad.stake(0, ethers.utils.parseEther("15"));
    await sAPPLaunchPad.connect(addr1).stake(0, ethers.utils.parseEther("15"));

    await network.provider.send("evm_increaseTime", [3600]);

    await sAPPLaunchPad.setWinners(0, [owner.address]);

    console.log(
      "winner",
      owner.address,
      " ",
      await sAPPLaunchPad.winners(0, owner.address)
    );
    console.log(
      "winner",
      owner.address,
      " ",
      await sAPPLaunchPad.winners(0, addr1.address)
    );

    await sAPPLaunchPad.connect(owner).claim(0);
    await sAPPLaunchPad.connect(addr1).unstake(0);
  });

  // Mint NFT (addr1) => List for sell (addr1) => addr2 send 1.5 eth to Treasury => admin buyAndBurn Token => admin mintByCrosschain to addr2 
  it("Bridge", async function () {
    const SAPPNftBridgeTreasury = await ethers.getContractFactory(
      "SAPPNftBridgeTreasury"
    );
    sAPPNftBridgeTreasury = await SAPPNftBridgeTreasury.deploy(
      sAPPNftMarketplace.address
    );
    await sAPPNftBridgeTreasury.deployed();

    await sAPPNftMarketplace
      .connect(addr1)
      .mint(addr1.address, ZERO_ADDRESS, "URI", 100, false);
    await sAPPNftMarketplace
      .connect(addr1)
      .setPriceAndSell(1, ethers.utils.parseEther("1"));

    await sAPPNftBridgeTreasury
      .connect(addr2)
      .pay(
        1,
        ethers.utils.parseEther("1"),
        ethers.utils.parseEther("1.5"),
        "97",
        "80001",
        {
          value: ethers.utils.parseEther("1.5"),
        }
      );

    await sAPPNftMarketplace.connect(owner).buyAndBurn(1, {
      value: ethers.utils.parseEther("1"),
    });

    await sAPPNftMarketplace.mintByCrosschain(
      addr2.address,
      ZERO_ADDRESS,
      "URI",
      100,
      addr1.address
    );
  });
});
