const { expect } = require("chai");
const { ethers } = require("hardhat");

describe("Greeter", function () {
  it("Should return the new greeting once it's changed", async function () {
    const Greeter = await ethers.getContractFactory("Greeter");
    const greeter = await Greeter.deploy("Hello, world!");
    await greeter.deployed();

    expect(await greeter.greet()).to.equal("Hello, world!");

    const setGreetingTx = await greeter.setGreeting("Hola, mundo!");

    // wait until the transaction is mined
    await setGreetingTx.wait();

  beforeEach(async function () {

    const SappNftMarketplace = await hre.ethers.getContractFactory(
      "SAPPNftMarketplace"
    );
    const Sapp = await SappNftMarketplace.deploy();
  
    await Sapp.deployed();
  
    console.log("SappNftMarketplace deployed to:", Sapp.address);
  
    const SappNftPool = await hre.ethers.getContractFactory("SAPPNftPool");
    const sappNftPool = await SappNftPool.deploy(Sapp.address);
  
    await sappNftPool.deployed();
  
    console.log("SappNftPool deployed to:", sappNftPool.address);
  
    await Sapp.setPool(sappNftPool.address);
  });


  describe("Deployment", function () {


    it("Should set onwer is Admin", async function () {
      expect(await Sapp.isAdmin(owner.address)).to.equal(true);
    });

    it("Should have name and symbol", async function () {
      expect(await Sapp.name()).to.equal("HaoToken");
      expect(await Sapp.symbol()).to.equal("HTK");
    });
  });

});
});
